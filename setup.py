#!/usr/bin/env python3
#
# This file is part of OnionSpoutsBot, a Tor Browser distribution Telegram bot.
#
# :authors: See AUTHORS file for more information.
#
# :copyright:   (c) 2020-2023, The OnionSproutsBot Authors.
#
# :license: This is Free Software. See LICENSE for license information.
#

import sys

from setuptools import find_packages, setup

assert sys.version_info >= (3, 6, 0)

import os
import subprocess
from pathlib import Path
from typing import List

from pkg_resources import parse_requirements

# TODO: Replace with `file:` directive in `setup.cfg` later. Support for the directive
#  was added in version 62.6, but our operating system target is Debian, and, as of the
#  time of this writing, that or any later version is not available.
#
#  See: https://setuptools.pypa.io/en/latest/userguide/declarative_config.html


current_dir = Path(__file__).parent
sys.path.insert(0, str(current_dir))


def get_install_requires() -> List[str]:
    with Path(current_dir / "requirements.txt").open() as requires:
        return [str(pkg) for pkg in parse_requirements(requires)]


# Compiles all the translations, creates a list of them and formats them so they can be accepted as data_files.
def create_catalogs():
    catalogs = []
    locale_dir = Path("locales")
    for lang in list(locale_dir.glob("*.po")):
        file_without_suffix = lang.with_suffix("")  # Used more than once
        # Just the locale name: "locales/onionsproutsbot+el" => "el"
        locale = file_without_suffix.name.split("+")[-1]
        # Output path: "locales/el/LC_MESSAGES/onionsproutsbot.mo"
        # with_name is being used cause we need the stem ("locales/")
        # can be replaced with `locale_dir, locale, ...`
        output = Path(
            file_without_suffix.with_name(locale), "LC_MESSAGES", "onionsproutsbot.mo"
        )
        output.parent.mkdir(parents=True, exist_ok=True)
        p = subprocess.run(
            ["msgfmt", str(lang), "-o", str(output)], capture_output=True
        )
        # msgfmt might error on warns, so better check if it compiled rather than depend on it.
        if output.exists():
            catalogs.append(
                (
                    os.path.join("OnionSproutsBot", "locales", locale, "LC_MESSAGES"),
                    [str(output)],
                )
            )
    return catalogs


setup(
    data_files=create_catalogs(),  # TODO: Replace data_files with package_data
    install_requires=get_install_requires(),
)
