#!/usr/bin/env python3
#
# This file is part of OnionSpoutsBot, a Tor Browser distribution Telegram bot.
#
# :authors: See AUTHORS file for more information.
#
# :copyright:   (c) 2020-2023, The OnionSproutsBot Authors.
#
# :license: This is Free Software. See LICENSE for license information.
#

platforms = {
    "linux32": "Linux 32-bit",
    "linux64": "Linux 64-bit",
    "macos": "macOS",
    "win32": "Windows 32-bit",
    "win64": "Windows 64-bit",
}

locales = {
    "en": "English",
    "en-us": "English",  # USA deliberately removed for UX purposes
    "en-uk": "English UK",
    "ar": "عربية",
    "ca": "Català",
    "cs": "česky",
    "da": "Dansk",
    "de": "Deutsch",
    "el": "Ελληνικά",
    "es": "Español",
    "es-es": "Español",
    "es-ar": "Español Arg.",
    "fa": "فارسی",
    "fr": "Français",
    "ga-ie": "Gaeilge",
    "he": "עברית",
    "hu": "Magyar nyelv",
    "id": "Indonesia",
    "is": "Islenska",
    "it": "Italiano",
    "ja": "日本語",
    "ka": "ქართული ენა",
    "ko": "한국어",
    "lt": "lietuvių kalba",
    "mk": "македонски",
    "ms": "بهاس ملايو",
    "my": "မြန်မာစာ",
    "nb-no": "Norsk Bokmål",
    "nl": "Nederlands",
    "pl": "Polszczyzna",
    "pt-br": "Português Brasil",
    "ro": "Română",
    "ru": "Русский",
    "sv-se": "Svenska",
    "th": "ภาษาไทย",
    "tr": "Türkçe",
    "vi": "Tiếng Việt",
    "zh-cn": "简体中文",
    "zh-tw": "正體字",
}
