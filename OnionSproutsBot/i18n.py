#!/usr/bin/env python3
#
# This file is part of OnionSpoutsBot, a Tor Browser distribution Telegram bot.
#
# :authors: See AUTHORS file for more information.
#
# :copyright:   (c) 2020-2023, The OnionSproutsBot Authors.
#
# :license: This is Free Software. See LICENSE for license information.
#

import gettext
import pkgutil
from pathlib import Path

import babel

import OnionSproutsBot

_locales_dir = Path(OnionSproutsBot.__path__[0] + "/locales")
if not _locales_dir.exists():
    _locales_dir = Path("locales")
_domain = "onionsproutsbot"


def setup_gettext():
    """
    Setups gettext text domains.
    """
    gettext.bindtextdomain(_domain, str(_locales_dir))
    gettext.textdomain(_domain)


def _get_available_langs():
    """
    Gets all available/translated languages by searching against babel's list.
    """
    langs = ["en"]
    languages = [*babel.Locale("en").languages, "zh-CN", "zh-TW"]
    for locale in gettext.find(
        _domain, str(_locales_dir), languages=languages, all=True
    ):
        parts = Path(locale).relative_to(str(_locales_dir)).parts
        # Assume there are at least 2 parts
        if len(parts) >= 2:
            langs.append(str(parts[0]))
    return langs


# TODO: It is likely that some leftover code that was uesful when multiple
# different versions of the Browser with different preinstalled locales existed
# can be found here.
def _get_full_names():
    """
    Gets a dictionary of {locale: { full_name, translation }}, where full_name is the
    name of the language in it's own language (eg. el_GR => Ελληνικά) and translation
    is the gettext translation (with fallback).
    """
    full_length_names = {}
    for locale in _get_available_langs():
        babel_locale = locale

        if locale == "zh-CN":
            babel_locale = "zh_hans"
        elif locale == "zh-TW":
            babel_locale = "zh_hant"

        full_length_names[locale.lower()] = {
            "full_name": babel.Locale.parse(babel_locale).get_display_name(
                babel_locale
            ),
            "translation": gettext.translation(
                _domain, str(_locales_dir), languages=[locale], fallback=True
            ),
        }
    return full_length_names


# This exists so the above two - expensive - functions, only run once on startup.
available_locales = _get_full_names()


def get_translation(locale="en"):
    """
    Gets the gettext#translation of the locale.
    """
    lang = locale.lower()
    if lang not in available_locales.keys():
        # If language is not translated, let gettext handle it.
        # Usually on start command & custom queries.
        return gettext.translation(
            _domain, str(_locales_dir), languages=[locale], fallback=True
        ).gettext
    return available_locales[lang]["translation"].gettext
