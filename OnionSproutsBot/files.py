#!/usr/bin/env python3
#
# This file is part of OnionSpoutsBot, a Tor Browser distribution Telegram bot.
#
# :authors: See AUTHORS file for more information.
#
# :copyright:   (c) 2020-2023, The OnionSproutsBot Authors.
#
# :license: This is Free Software. See LICENSE for license information.
#

import asyncio
import random
import time

from aiofiles.tempfile import NamedTemporaryFile
from aiohttp import ClientSession
from pyrogram import Client
from pyrogram.errors.exceptions.flood_420 import FloodWait
from pyrogram.types import Message

from OnionSproutsBot import dialogue, i18n

from .i18n import available_locales

"""
It is assumed that all of these functions will only be
executed only after the bot has been initialized.

By "initialized", it is meant that the bot has been
configured with a configuration file and successfully
connected to Telegram's servers.
"""


def duration(time: int, language: str) -> str:
    days = int((time / 86400))
    hours = int((time / 3600) % 24)
    minutes = int((time / 60) % 60)
    seconds = int(time % 60)
    lang = available_locales[language]["translation"]

    if not days and not hours and not minutes:
        return lang.ngettext("%d second", "%d seconds", seconds) % (seconds,)
    else:
        need_comma = False
        buffer = ""

        if days:
            buffer = lang.ngettext("%d day", "%d days", days) % (days,)
            need_comma = True
        if hours:
            buffer += (", " if need_comma else "") + lang.ngettext(
                "%d hour", "%d hours", hours
            ) % (hours,)
            need_comma = True
        if minutes:
            buffer += (", " if need_comma else "") + lang.ngettext(
                "%d minute", "%d minutes", minutes
            ) % (minutes,)
            need_comma = True
        if seconds:
            buffer += (", " if need_comma else "") + lang.ngettext(
                "%d second", "%d seconds", seconds
            ) % (seconds,)

        return buffer


def format_bytes(size) -> str:
    size = int(size)
    power_labels = {0: "B", 1: "KiB", 2: "MiB", 3: "GiB", 4: "TiB"}
    n = 0
    # Doing 1024 may round off partial sizes (eg, 1.5 GiB -> 1 GiB)
    while size > 2048:
        size >>= 10
        n += 1
    return f"{size} {power_labels[n]}"


def calculate_eta(current: int, total: int, start_time: int, current_time: int) -> int:
    if not current:
        return 0

    elapsed_time = current_time - start_time
    return (elapsed_time * (total / current)) - elapsed_time


# https://stackoverflow.com/a/34325723
def generate_bar(current: int, total: int) -> str:
    filled_length = int(30 * current // total)
    return "[" + "=" * filled_length + " " * (30 - filled_length) + "]"


progress_callback_data = {}


async def progress(current: int, total: int, reply: Message, upload: bool, lang: str):
    _ = i18n.get_translation(lang)
    dict_id = (reply.chat.id, reply.id)
    current_time = int(time.time())
    last_edit, prevtext, start_time = progress_callback_data.get(
        dict_id, (0, None, current_time)
    )

    if current == total:
        # upload complete
        try:
            progress_callback_data.pop(dict_id)
        except KeyError:
            pass
    elif (current_time - last_edit) > random.randint(1, 9):
        if last_edit:
            speed = format_bytes((total - current) / (current_time - start_time))
        else:
            speed = "0 B"

        percentage = (current / total) * 100.0
        eta = calculate_eta(current, total, start_time, current_time)

        text = "<b>" + _("We are preparing your download...") + "</b>\n\n"

        if upload:
            text += _(
                "We're now sending the file to Telegram, it will be ready in %s!"
            ) % duration(eta, lang)
        else:
            text += _(
                "You're the first person to request this file, so we have to download it first! "
                "Please wait..."
            )

        text += f"\n\n<code>{generate_bar(current, total)} {percentage:.2f}% ({speed}/s)</code>"

        if prevtext != text:
            try:
                await reply.edit(text)
            except FloodWait:
                # Skip this edit
                pass
            prevtext = text
            last_edit = current_time
            progress_callback_data[dict_id] = (last_edit, prevtext, start_time)


async def relay_files(
    client: Client,
    callback: str,
    url: str,
    original_name: str,
    directory: str,
    reply: Message,
    lang: str,
) -> str:
    try:
        size = 0
        async with ClientSession(raise_for_status=True) as session:
            async with NamedTemporaryFile(mode="wb", dir=directory) as file:
                written = 0
                async with session.get(url, timeout=600) as resp:
                    if not size:
                        size = int(resp.headers.get("content-length", 0)) or 0
                    async for chunk in resp.content.iter_chunked(2048):
                        written += len(chunk)
                        await progress(written, size, reply, False, lang)
                        await file.write(chunk)
                await file.seek(0)

                # Always try to send documents, even during a floodwait.
                while True:
                    try:
                        sent_file = await client.send_document(
                            callback.from_user.id,
                            document=file.name,
                            file_name=original_name,
                            progress=progress,
                            progress_args=(reply, True, lang),
                        )
                        break
                    except FloodWait as ex:
                        await asyncio.sleep(ex.x + 1)

                await reply.delete()
                return sent_file.document.file_id
    except Exception as e:
        dict_id = (reply.chat.id, reply.id)
        if dict_id in progress_callback_data:
            progress_callback_data.pop(dict_id)

        await reply.delete()
        raise e
