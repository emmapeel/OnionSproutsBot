Although OnionSproutsBot is a project that is developed independently
from The Tor Project, Inc., one of its goals is to integrate itself
enough to the organization in order to ensure its longevity and
usefulness.

Even if the majority of this project's authors do not have any business
relationship with The Tor Project, Inc., the project still falls under
what would be best described as the community surrounding the
aforementioned organization. The maintainer also believes in the
values represented by the organization and its community, which aim to
foster an inclusive community where people feel safe to participate,
engage and share their points of view.

For those reasons, the maintainer believes that OnionSproutsBot should
use the Code of Conduct of The Tor Project, Inc. This does not mean
that third parties will necessarily get involved in the resolution of
a supposed interpersonal conflict; you are merely reminded that even if
we are independent, we have a similar belief system and that you should
choose to be awesome to your fellow human beings whenever possible.

The latest version of the Code of Conduct can be found here:

https://gitweb.torproject.org/community/policies.git/plain/code_of_conduct.txt
