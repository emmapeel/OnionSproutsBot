#!/usr/bin/env python3
#
# This file is part of OnionSpoutsBot, a Tor Browser distribution Telegram bot.
#
# :authors: See AUTHORS file for more information.
#
# :copyright:   (c) 2020-2023, The OnionSproutsBot Authors.
#
# :license: This is Free Software. See LICENSE for license information.
#

from typing import Optional

from pyrogram import Client
from pyrogram.types import (CallbackQuery, InlineKeyboardButton,
                            InlineKeyboardMarkup)

from OnionSproutsBot import i18n


# Example query: request_tor_mirrors:en
async def send_mirrors(client: Client, callback: CallbackQuery):
    callback_data = callback.data.split(":")
    lang = callback_data[-1]
    _ = i18n.get_translation(lang)

    await client.send_message(
        chat_id=callback.from_user.id,
        text="<b>" + _("Tor Browser Mirrors") + "</b>\n\n"
        "- EFF: https://tor.eff.org\n"
        "- Calyx Institute: https://tor.calyxinstitute.org\n"
        "- GitHub: https://github.com/torproject/torbrowser-releases/releases/\n"
        "- Email: gettor@torproject.org "
        + _("(You should also include your operating system: Windows, macOS, or Linux)")
        + "\n\n"
        + _("(Make sure that your email provider is safe!)")
        + "\n",
        reply_markup=InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton(
                        text=_("Go Back"), callback_data="send_faq:" + lang
                    )
                ],
                [
                    InlineKeyboardButton(
                        text=_("Main Menu"), callback_data="welcome:" + lang
                    )
                ],
            ]
        ),
    )

    await callback.answer()


# Example query: request_tor_bridges:en
async def send_bridges(client: Client, callback: CallbackQuery):
    callback_data = callback.data.split(":")
    lang = callback_data[-1]
    _ = i18n.get_translation(lang)
    bridge_username = "@getbridgesbot"

    await client.send_message(
        chat_id=callback.from_user.id,
        text="<b>"
        + _("Tor Bridges")
        + "</b>\n\n"
        + _("You can request a bridge from {username}. Please keep in mind "
            "that you will also have to configure the Tor Browser accordingly "
            "in order to use it."
        ).format(username=bridge_username),
        reply_markup=InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton(
                        text=_("Wait, what are Tor bridges?"),
                        callback_data="explain_bridges:" + lang,
                    )
                ],
                [
                    InlineKeyboardButton(
                        text=_("Go Back"), callback_data="send_faq:" + lang
                    )
                ],
                [
                    InlineKeyboardButton(
                        text=_("Main Menu"), callback_data="welcome:" + lang
                    )
                ],
            ],
        ),
    )

    await callback.answer()


# Example query: explain_tor:en
async def explain_tor(client: Client, callback: CallbackQuery):
    callback_data = callback.data.split(":")
    lang = callback_data[-1]
    _ = i18n.get_translation(lang)

    await client.send_message(
        chat_id=callback.from_user.id,
        text="<b>"
        + _("What is Tor?")
        + "</b>\n\n"
        + _("Tor is a network that lets you anonymously connect to the Internet, "
            "while evading censorship, people monitoring of your Internet connection "
            "and ad tracking."
        )
        + "\n\n"
        + _("The network is peer-to-peer and largely supported by volunteers, as "
            "well as The Tor Project Inc., a non-profit organization based "
            "in the United States."
        ),
        reply_markup=InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton(
                        text=_("Go Back"), callback_data="send_faq:" + lang
                    )
                ],
                [
                    InlineKeyboardButton(
                        text=_("Main Menu"), callback_data="welcome:" + lang
                    )
                ],
            ]
        ),
    )

    await callback.answer()


# Example query: explain_bridges:en
async def explain_bridges(client: Client, callback: CallbackQuery):
    callback_data = callback.data.split(":")
    lang = callback_data[-1]
    _ = i18n.get_translation(lang)

    await client.send_message(
        chat_id=callback.from_user.id,
        text="<b>"
        + _("What are Tor bridges?")
        + "</b>\n\n"
        + _("Tor bridges are computers that can be very helpful when "
            "your internet provider has blocked you from using Tor, "
            "or if you just want to hide the fact that you are using Tor "
            "from anyone that could be monitoring your internet connection. "
            "That could be your internet provider, government, household "
            "members, school, workplace, among others.")
        + "\n\n"
        + _("If the Tor network is blocked in your country, you can use "
            "a bridge to gain access again. Bridges are also a good idea to "
            "use if you want to use Tor in a discrete manner."),
        reply_markup=InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton(
                        text=_("Cool, how do I get one?"),
                        callback_data="request_tor_bridges:" + lang,
                    )
                ],
                [
                    InlineKeyboardButton(
                        text=_("Go Back"), callback_data="send_faq:" + lang
                    )
                ],
                [
                    InlineKeyboardButton(
                        text=_("Main Menu"), callback_data="welcome:" + lang
                    )
                ],
            ]
        ),
    )

    await callback.answer()


# Example query: request_support:en
async def send_support(client: Client, callback: CallbackQuery):
    callback_data = callback.data.split(":")
    lang = callback_data[-1]
    _ = i18n.get_translation(lang)
    support_username = "@TorProjectSupportBot"
    forum_link = "https://forum.torproject.net"

    await client.send_message(
        chat_id=callback.from_user.id,
        text="<b>"
        + _("Need support?")
        + "</b>\n\n"
        + _("If you need support with anything that has to do with Tor, the Tor "
            "user support team can be reached on Telegram: {username}.\n\n"
            "There's also a dedicated active forum where anyone can post "
            "a question, feedback, or concern: {link}"
        ).format(username=support_username, link=forum_link),
        reply_markup=InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton(
                        text=_("Go Back"), callback_data="send_faq:" + lang
                    )
                ],
                [
                    InlineKeyboardButton(
                        text=_("Main Menu"), callback_data="welcome:" + lang
                    )
                ],
            ]
        ),
    )

    await callback.answer()
