#!/usr/bin/env python3
#
# This file is part of OnionSpoutsBot, a Tor Browser distribution Telegram bot.
#
# :authors: See AUTHORS file for more information.
#
# :copyright:   (c) 2020-2023, The OnionSproutsBot Authors.
#
# :license: This is Free Software. See LICENSE for license information.
#

__version__ = "1.2.1"
