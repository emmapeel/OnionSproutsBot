#!/usr/bin/env python3
#
# This file is part of OnionSpoutsBot, a Tor Browser distribution Telegram bot.
#
# :authors: See AUTHORS file for more information.
#
# :copyright:   (c) 2020-2023, The OnionSproutsBot Authors.
#
# :license: This is Free Software. See LICENSE for license information.
#

import asyncio
import logging
import os
import sys
import urllib
from datetime import datetime as dt
from distutils.util import get_platform
from pathlib import Path
from time import time
from typing import Union

import aiosqlite
import requests
import yaml
from pyrogram import Client, filters, idle
from pyrogram.errors.exceptions.bad_request_400 import QueryIdInvalid, WebpageCurlFailed
from pyrogram.types import (
    CallbackQuery,
    InlineKeyboardButton,
    InlineKeyboardMarkup,
    InlineQueryResultArticle,
    InputTextMessageContent,
    KeyboardButton,
    Message,
    ReplyKeyboardMarkup,
)
from requests import HTTPError

from . import database, dialogue, files, helpers, i18n, titles, ui

# TODO: Better argument interface
with open(sys.argv[1], "r") as config:
    data = yaml.safe_load(config)

Bot = Client(
    "OnionSproutsBot",
    data["telegram"]["api_id"],
    data["telegram"]["api_hash"],
    bot_token=data["telegram"]["bot_token"],
    in_memory=True,
)

i18n.setup_gettext()


@Bot.on_message(filters.command("start"))
async def start_command(client: Client, message: Union[Message, CallbackQuery]):
    user = message.from_user
    # User is not provided if the message is in a channel.
    if not user:
        return None

    # Check if the language returned by Telegram is available.
    # If it is, send the welcome message, else send the language menu.
    user_lang = user.language_code
    if user_lang is None:
        user_lang = "en"
    else:
        user_lang = user_lang.lower()

    available_locales = i18n.available_locales.keys()
    if user_lang != "en" and user_lang in available_locales:
        return await send_welcome_message(client, message.from_user.id, user_lang)

    await send_language_menu(client, message.chat.id, user_lang)


# Sends the lanuage menu
async def send_language_menu(client: Client, chat_id: int, user_lang: str):
    # Set the translation to user_lang.
    _ = i18n.get_translation(user_lang)

    lang_rows = []
    available_locales = i18n.available_locales.keys()
    # The sort function is being used to reorder the list so user.language_code
    # is the first button, in a way to make it easier for the user to pick
    # their language if Telegram returned the correct one.
    for lang in sorted(available_locales, key=user_lang.__eq__, reverse=True):
        lang_rows.append(
            InlineKeyboardButton(
                text=f'{i18n.available_locales[lang]["full_name"]} ({lang})',
                callback_data="welcome:" + lang,
            )
        )

    button_rows = ui.get_rows(lang_rows, 3)

    lang_markup = InlineKeyboardMarkup(button_rows)

    try:
        await client.send_photo(
            chat_id=chat_id,
            photo=data["bot"]["language_photo"],
            caption="<b>"
            + _("Language selection")
            + "</b>\n\n"
            + _("Please select your language:"),
            reply_markup=lang_markup,
        )
    except (WebpageCurlFailed, KeyError):
        await client.send_message(
            chat_id=chat_id,
            text="<b>"
            + _("Language selection")
            + "</b>\n\n"
            + _("Please select your language:"),
            reply_markup=lang_markup,
        )


# Example query: change_lang:en
@Bot.on_callback_query(filters.regex("^change_lang:[^:]+$"))
async def change_lang(client: Client, callback: CallbackQuery):
    # All callbacks need to get the language from now on.
    callback_data = callback.data.split(":")
    lang = callback_data[-1]

    await send_language_menu(client, callback.from_user.id, lang)
    await callback.answer()


# Example query: welcome:en
@Bot.on_callback_query(filters.regex("^welcome:[^:]+$"))
async def welcome_command(client: Client, callback: CallbackQuery):
    # All callbacks need to get the language from now on.
    callback_data = callback.data.split(":")
    lang = callback_data[-1]

    await send_welcome_message(client, callback.from_user.id, lang)
    await callback.answer()


# Sends the welcome message
async def send_welcome_message(client: Client, user_id: int, lang: str):
    _ = i18n.get_translation(lang)
    bot_name = _("GetTor")

    # If lang is English, label = 'Change Language 🌐'
    # else label = "<'Change Language' translated> (Change Language) 🌐"
    if lang.startswith("en_") or lang == "en":
        change_lang_button_label = "Change Language 🌐"
    else:
        change_lang_button_label = _("Change Language") + " (Change Language) 🌐"

    await client.send_message(
        chat_id=user_id,
        text="<b>"
        + _("Welcome!")
        + "</b>\n\n"
        + _("Hi, welcome to {name}! ").format(name=bot_name)
        + _("What do you want me to do?"),
        reply_markup=InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton(
                        _("Send me the Tor Browser"), "request_tor:" + lang
                    )
                ],
                [InlineKeyboardButton(change_lang_button_label, "change_lang:" + lang)],
                [InlineKeyboardButton(_("More..."), "send_faq:" + lang)],
            ]
        ),
    )


@Bot.on_callback_query(filters.regex("^send_faq:[^:]+$"))
async def send_faq(client: Client, callback: CallbackQuery):
    callback_data = callback.data.split(":")
    lang = callback_data[-1]
    _ = i18n.get_translation(lang)

    await client.send_message(
        chat_id=callback.from_user.id,
        text="<b>" + _("More...") + "</b>\n\n" + _("What do you want to know?"),
        reply_markup=InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton(
                        _("Send me other mirrors for the Tor Browser"),
                        "request_tor_mirrors:" + lang,
                    )
                ],
                [
                    InlineKeyboardButton(
                        _("Send me Tor bridges."),
                        "request_tor_bridges:" + lang,
                    )
                ],
                [
                    InlineKeyboardButton(
                        _("Explain what Tor bridges are."),
                        "explain_bridges:" + lang,
                    )
                ],
                [
                    InlineKeyboardButton(
                        _("Explain what Tor is."),
                        "explain_tor:" + lang
                    )
                ],
                [
                    InlineKeyboardButton(
                        _("I have other questions."),
                        "request_support:" + lang
                    )
                ],
            ]
        ),
    )


# Example query: request_tor:en
@Bot.on_callback_query(filters.regex("^request_tor:[^:]+$"))
async def request_tor(client: Client, callback: CallbackQuery):
    global response

    callback_data = callback.data.split(":")
    lang = callback_data[-1]
    _ = i18n.get_translation(lang)

    platform_keyboard = []
    # Update check on new flow.
    response = await helpers.get_response(data["tor"]["endpoint"])

    # Generate list of available platforms, then put
    # each of them in separate buttons.
    for platform in ui.get_platform_list(response):
        platform_keyboard.append(
            InlineKeyboardButton(
                text=titles.platforms.get(platform.lower(), platform),
                # Here, "select_locale" may be a bit confusing,
                # but here, the buttons tell the bot to proceed
                # with select_locale, while telling it the platform that
                # was chosen.
                callback_data="download_tor:" + platform + ":" + lang,
            )
        )

    # After we collect all of the buttons that we should we have,
    # we create our keyboard, and then send it to the user.
    button_rows = ui.get_rows(platform_keyboard, 2)
    platform_markup = InlineKeyboardMarkup(button_rows)

    await client.send_message(
        callback.from_user.id,
        text="<b>"
        + _("Download Tor from Telegram")
        + "</b>\n\n"
        + _("Which operating system are you using?"),
        reply_markup=platform_markup,
    )

    await callback.answer()


# Example query: download_tor:linux64:it:en
@Bot.on_callback_query(filters.regex("^download_tor:[^:]+:[^:]+$"))
async def download_tor(client: Client, callback: CallbackQuery):
    callback_data = callback.data.split(":")
    lang = callback_data[-1]
    platform = callback_data[1]
    locale = "ALL"  # Tor ships multilingual binaries
    _ = i18n.get_translation(lang)

    logging.info(f"Binary ({platform}) requested.")
    logging.debug(f"Request: {platform} ({lang}) - Callback: {callback_data}")

    # Checks if the requested platform and locale exist. If not, the
    # whole process is aborted for security reasons.
    if (platform not in ui.get_platform_list(response)) or (
        locale not in ui.get_locale_list(response, platform)
    ):
        logging.critical(f"Binary ({platform}) NOT in list. Aborting...")
        return await callback.answer()

    # Fetch the download link for the requested files with the response.
    # We do this regardless of whether we already have cached the files
    # or not, as we cannot easily predict the file name and submit a
    # request anyways.
    tor_sig_url = response["downloads"][platform][locale]["sig"]
    tor_bin_url = response["downloads"][platform][locale]["binary"]

    logging.debug(f"Requested signature URL: {tor_sig_url}")
    logging.debug(f"Requested binary URL: {tor_bin_url}")

    tor_sig_original_name = tor_sig_url.rsplit("/")[-1]
    tor_bin_original_name = tor_bin_url.rsplit("/", 1)[-1]

    logging.debug(f"Requested signature name: {tor_sig_original_name}")
    logging.debug(f"Requested binary name: {tor_bin_original_name}")

    # We check if the file has already been uploaded to Telegram. If so,
    # there should be an entry in our database for it.
    results = await database.search_file_in_db(curs, tor_bin_original_name)

    if results != None:
        logging.info(f"Binary ({platform}) found!")
        logging.debug(f"Found binary cache ID: {results[1]}")
        logging.debug(f"Found signature cache ID: {results[3]}")

        # 1: File, 3: Signature
        await client.send_cached_media(callback.from_user.id, file_id=results[1])
        await client.send_cached_media(callback.from_user.id, file_id=results[3])
        await helpers.upload_already_done(client, callback, lang, platform)
    else:
        try:
            logging.info(f"Binary ({platform}) not found. Uploading...")

            await client.send_message(
                callback.from_user.id,
                _(
                    "The version you have requested **(%s)** "
                    "hasn't been uploaded to Telegram's servers yet. "
                    "Please wait..."
                )
                % (platform),
            )

            reply = await helpers.sig_send_notification(client, callback, lang)

            tor_sig_id = await files.relay_files(
                client,
                callback,
                tor_sig_url,
                tor_sig_original_name,
                data["bot"]["download_path"],
                reply,
                lang,
            )

            reply = await helpers.file_send_notification(client, callback, lang)

            tor_bin_id = await files.relay_files(
                client,
                callback,
                tor_bin_url,
                tor_bin_original_name,
                data["bot"]["download_path"],
                reply,
                lang,
            )

            logging.info(f"--- NEW ENTRY ---")
            logging.info(f"Binary name: {tor_bin_original_name}")
            logging.info(f"Binary cache ID: {tor_bin_id}")
            logging.info(f"Signature name: {tor_sig_original_name}")
            logging.info(f"Signature cache ID: {tor_sig_id}")

            await database.insert_new_release(
                curs,
                conn,
                tor_bin_original_name,
                tor_bin_id,
                tor_sig_original_name,
                tor_sig_id,
            )

            logging.info(f"Database entry: Successful")
            logging.info(f"-----------------")
            await helpers.upload_succeeded(client, callback, lang, platform)

        except Exception as e:
            # Explicitly send a message to notify them of the failure.
            logging.exception(e)
            logging.info(f"Database entry: Likely failed")
            logging.info(f"-----------------")
            await helpers.upload_failed(client, callback, lang, e, platform)
    try:
        await callback.answer()
    except QueryIdInvalid as e:
        logging.warning(f"Error answering callback: {e}")


# TODO: Replace this horrifying hack with a proper plugin system
Bot.on_callback_query(filters.regex("^request_tor_mirrors:[^:]+$"))(
    dialogue.send_mirrors
)

Bot.on_callback_query(filters.regex("^request_tor_bridges:[^:]+$"))(
    dialogue.send_bridges
)

Bot.on_callback_query(filters.regex("^explain_tor:[^:]+$"))(
    dialogue.explain_tor
)

Bot.on_callback_query(filters.regex("^explain_bridges:[^:]+$"))(
    dialogue.explain_bridges
)

Bot.on_callback_query(filters.regex("^request_support:[^:]+$"))(
    dialogue.send_support
)

async def main():
    global conn, curs, response

    """
    We take care of all of the logging stuff before we
    do anything else. There definitely has to be some sort
    of a filename though, so we hardcode "OSB" in case
    nothing else is available.
    """

    # We start off with the path where files will be stored.
    # If one hasn't been defined, it will be left blank and
    # the default directory will be the current working one.
    log_filename = str(data["logging"]["directory"] or "")

    # Carrying on with the file name...
    if data["logging"]["filename_suffix"] is not None:
        log_filename += data["logging"]["filename_suffix"]
    else:
        log_filename += "OSB"

    # We concatenate the date and the time for better organization...
    # As always, this is optional, but configured by default.
    if data["logging"]["filename_datefmt"] is not None:
        # Adds separator between name and date, if defined.
        log_filename += str(data["logging"]["date_separator"] or "")
        # Adds date depending on format defined in the configuration.
        log_filename += dt.now().strftime(data["logging"]["filename_datefmt"])

    logging.basicConfig(
        filename=f'{log_filename}.log',
        format=data['logging']['format'],
        encoding=data['logging']['encoding'],
        level=logging.INFO,
        datefmt=f"{data['logging']['datefmt']}"
    )
    logging.getLogger().addHandler(logging.StreamHandler()) # prints to stderr

    # Okay, here comes the real deal.
    logging.info("=== OnionSproutsBot ===")

    # Custom paths
    if data["bot"]["download_path"] is not None:
        logging.info(f"Custom download path: {data['bot']['download_path']}")

    try:
        if data["bot"]["db_path"] is None:
            logging.info(f"Database path: {os.getcwd()}/{data['bot']['db_name']}")
            conn = await aiosqlite.connect(f"{os.getcwd()}/{data['bot']['db_name']}")
        else:
            conn = await aiosqlite.connect(data["bot"]["db_path"])
    except Exception as e:
        logging.warning(e)
        exit(1)

    curs = await conn.cursor()

    await database.create_empty_db(curs, conn)

    # Obtains a list of available downloads on startup.
    response = requests.get(data["tor"]["endpoint"]).json()
    logging.info("Initial response from endpoint obtained.")

    await conn.commit()
    logging.info("Connection to database initiated.")

    # Call Pyrogram's idle forever function
    await Bot.start()
    await idle()
    await conn.close()


loop = asyncio.get_event_loop()


def init():
    loop.run_until_complete(main())
