#!/usr/bin/env python3
#
# This file is part of OnionSpoutsBot, a Tor Browser distribution Telegram bot.
#
# :authors: See AUTHORS file for more information.
#
# :copyright:   (c) 2020-2023, The OnionSproutsBot Authors.
#
# :license: This is Free Software. See LICENSE for license information.
#

# TODO: Update platform lists every now and then asynchronously, cache
#  the returned values of the functions.


def get_platform_list(response):
    """
    Iterates the response and returns the lowercased
    platforms.
    """
    platforms = [platform for platform in response["downloads"].keys()]
    return platforms


def get_locale_list(response, platform):
    """
    Iterates the response and returns a list of locales
    according to the BCP 47 standard.
    """
    # We do not issue two requests, as that would be a waste
    # of resources. We assume that all locales which exist
    # for one platform are also available in other platforms.
    locales = [locale for locale in response["downloads"][platform].keys()]
    return locales


def get_rows(buttons, items_per_row):
    """
    get_rows splits the buttons list into
    smaller ones based on the amount items you
    want per row.

    :param buttons: List of InlineKeyboardButton
    :param items_per_row: Int
    :return: List of List of InlineKeyboardButton
    """
    result = []
    tmp = []
    for index, button in enumerate(buttons):
        tmp.append(buttons[index])
        if (index + 1) % items_per_row == 0:
            result.append(tmp)
            tmp = []

    if len(tmp) > 0:
        result.append(tmp)

    return result
