#!/bin/bash

if [ -d "locales" ]; then
    if [ -d "OnionSproutsBot" ]; then
      echo "Already possibly in top directory, running xgettext..."
    else
      echo "Not in directory, exiting..."
      exit 1
    fi
else
  if [ -d "../locales" ]; then
    cd ..
  else
    echo "Not in directory, exiting..."
    exit 1
  fi
fi

echo "Exporting strings from Python source files..."

xgettext -L Python \
  --files-from=locales/POTFILES \
  --output=locales/onionsproutsbot.pot

echo "Done."
exit 0
